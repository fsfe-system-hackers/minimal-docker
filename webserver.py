#!/usr/bin/env python3
"""A web server to echo back a request's headers and data."""

# SPDX-FileCopyrightText: 2020 Nick Janetakis <nick.janetakis@gmail.com>
# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
# SPDX-License-Identifier: MIT

# Usage: python3 webserver.py
#        python3 webserver.py localhost:12345
#        python3 webserver.py 0.0.0.0:80

# Loosely based on https://github.com/nickjj/webserver, thank you!


from http.server import HTTPServer, BaseHTTPRequestHandler
import sys

# Default binds
BIND_HOST = "localhost"
PORT = 8080


class EchoHeaderRequestHandler(BaseHTTPRequestHandler):
    """A very simple handler echoing HTTP request headers"""

    def do_GET(self):  # pylint: disable=invalid-name
        """Handle GET requests"""
        self.write_response()

    def do_POST(self):  # pylint: disable=invalid-name
        """Handle POST requests"""
        self.write_response()

    def write_response(self):
        """Compose and send the echoed headers"""
        # compose reply
        headers = self.requestline
        headers += f"\nClient-Address: {self.client_address[0]}\n"
        headers += str(self.headers)

        # encode as utf-8 bytes
        headers = bytes(headers, encoding="utf-8")

        self.send_response(200)
        self.end_headers()
        self.wfile.write(headers)


# Take provided host:port argument
if len(sys.argv) == 2:
    arg = sys.argv[1].split(":")
    BIND_HOST = arg[0]
    PORT = int(arg[1])
# We only take one argument
elif len(sys.argv) > 2:
    print("Too many arguments given!")
    sys.exit(1)

# Initial log message
print(f"Listening on http://{BIND_HOST}:{PORT}\n")

# Start HTTP server
httpd = HTTPServer((BIND_HOST, PORT), EchoHeaderRequestHandler)
httpd.serve_forever()
