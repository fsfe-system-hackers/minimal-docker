---
title: Example documentation
description: Just a page to show how Markdown pages in the repo are transformed look on https://docs.fsfe.org
---

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# What this is good for

This folder can the markdown files it contains can be used for in-depth,
wiki-like documentation that is then also displayed on https://docs.fsfe.org. 



