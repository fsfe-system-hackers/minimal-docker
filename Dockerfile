# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

# Define base image
FROM bitnami/python:3.11

# Put webserver script in place
COPY webserver.py /app/

# Run webserver, open for all outside requests
CMD ["python3", "/app/webserver.py", "0.0.0.0:12345"]
