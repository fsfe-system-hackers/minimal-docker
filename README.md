<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V.

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Minimal Docker example

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/minimal-docker/00_README)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/minimal-docker/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/minimal-docker)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/minimal-docker)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/minimal-docker)

This repo contains a rather minimalistic example of how a Docker service can
look like in our infrastructure. It also shows how it's deployed via Drone.

It complements the [technical documentation on Docker
deployments](https://wiki.fsfe.org/TechDocs/TechnicalProcesses/DockerDeployment)
for the FSFE System Hackers.

## Important files

* `.drone.yml` is picked up by our [Drone CI](https://drone.fsfe.org), and
  executes the listed steps:
  * Check REUSE compliance
  * Run docker-compose to deploy the Docker container as defined, and only in
    defined events
  * On a defined docker host
  * For a rootless Docker environment
* `docker-compose.yml` defines which containers are going to be started, and how
  they are described.
  * In this example very simple: building a Docker image based on the
    `Dockerfile`
  * It also exposes ports to the host and sets labels that allow our reverse
    proxy to channel incoming requests to the container
* `Dockerfile` defines the layers of the Docker image `minimaldocker` that will
  be deployed.
  * Based on a well-maintained Python image
  * Running a very simple webserver that echoes HTTP requests
